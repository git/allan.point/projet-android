package  projet.iut.jeu_de_la_vie.model;

/**
 * Toutes les règles disponibles
 */
public enum Regle {
	/**
	 * La cellule nait si elle a exactement 3 voisin
	 * Elle reste en vie avec 2 ou 3 voisin
	 * Elle meurt dans les autres sitations
	 */
	CONWAY_STYLE,

	/**
	 * Toutes les cellules meurts
	 */
	REGLE_KILLER
}
