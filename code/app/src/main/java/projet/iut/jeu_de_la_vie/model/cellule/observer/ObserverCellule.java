package projet.iut.jeu_de_la_vie.model.cellule.observer;

import projet.iut.jeu_de_la_vie.model.cellule.Cellule;

/**
 * Permet d'obbserver une boucle de jeu
 * @author Yohann Breuil
 * @author Allan Point
 */
public interface ObserverCellule {
	/**
	 * Réaction en cas de notification
	 */
	void update(Cellule cellule);
}
