package projet.iut.jeu_de_la_vie.model.cellule.créateur;

import projet.iut.jeu_de_la_vie.model.cellulesVivantes.CellulesVivantes;
import projet.iut.jeu_de_la_vie.model.cellule.Cellule;

import java.util.List;

/**
 * Couche d'abstraction de la création des cellules
 * @author Yohann Breil
 * @author Allan Point
 */
public interface ICreateurCellule {

	/**
	 * Créer des cellules selon les dimentions précisé dans le constructeur.
	 * @param observer Permet d'abonner un objet CellulesVivantes à toute les cellules.
	 * @return Une liste observable pour fxml avec toutes les cellules standardisées  .
	 */
	List<List<Cellule>> creerCellules(CellulesVivantes observer);
}
