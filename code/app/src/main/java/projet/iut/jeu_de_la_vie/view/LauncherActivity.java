package projet.iut.jeu_de_la_vie.view;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import projet.iut.jeu_de_la_vie.R;
import projet.iut.jeu_de_la_vie.model.Manager;
import projet.iut.jeu_de_la_vie.model.cellule.Cellule;
import projet.iut.jeu_de_la_vie.view.fragment.FragmentPlateau;
import projet.iut.jeu_de_la_vie.view.fragment.MenuFragment;

public class LauncherActivity extends AppCompatActivity {

    private Manager manager;
    public static int width;
    public static int height;
    public static Boolean isPortrait = true;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        manager = new Manager();
        setContentView(R.layout.jeu_de_la_vie);
        DisplayMetrics disp = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(disp);
        width = disp.widthPixels;
        height = disp.heightPixels;
    }

    @Override
    protected void onStart (){
        super.onStart();
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT){
            isPortrait = true;
        }
        else if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
            isPortrait = false;
        }
    }

    @Override
    public void onBackPressed() {
        getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainerViewId, MenuFragment.class,null).commit();
        Log.d("Press", "Back Arrow");
    }

    private void setCellsColor(int color){
        manager.setCellsColor(color);
    }

    public void setCellsRed(View view){
        int red = Color.RED;
        setCellsColor(red);
    }
    public void setCellsGreen(View view){
        int green = Color.GREEN;
        setCellsColor(green);
    }
    public void setCellsBlue(View view){
        int blue = Color.BLUE;
        setCellsColor(blue);
    }

    public void startGame(Bundle bundle){
        Log.d("fragment", "" + (manager.getNumberOfLines()));
        Log.d("fragment", "" + (manager.getNomberOfColumns()));
        Log.d("fragment", "" + Cellule.getLivingColor());
        getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainerViewId, FragmentPlateau.class,bundle).commit();
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("manager",manager); //c'est bourrin mais magique
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        manager = (Manager) savedInstanceState.getSerializable("manager");
    }

}
