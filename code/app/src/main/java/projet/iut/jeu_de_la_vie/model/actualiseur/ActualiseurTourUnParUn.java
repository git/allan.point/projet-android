package projet.iut.jeu_de_la_vie.model.actualiseur;


/**
 * Actualiseur de tours qui incrémente le numéro de génération 1 par 1
 * @author Yohann Breuil
 * @author Allan Point
 */
public class ActualiseurTourUnParUn implements ActualiseurTour{

	private int cptTour ;
		public int getcptTour(){return cptTour;}
		private void setCptTour(int valeur){cptTour =valeur;}

	public ActualiseurTourUnParUn(){
		resetTour();
	}

	/**
	 * Incrémneter le numéro de génération de 1
	 */
	@Override
	public void changerTour() {
		setCptTour(getcptTour()+1);
	}

	/**
	 * Réinitialiser le numéro de génération à 0
	 */
	@Override
	public void resetTour(){
		setCptTour(0);
	}
}
