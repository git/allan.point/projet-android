package projet.iut.jeu_de_la_vie.model.actualiseur;

import projet.iut.jeu_de_la_vie.model.cellulesVivantes.CellulesVivantes;
import projet.iut.jeu_de_la_vie.model.arbitre.Arbitre;

/**
 * Gère l'actualisation des cellules (Abstraction)
 * @author Yohann Breuil
 * @author Allan Point
 */
public abstract class ActualiseurCellule {
	private Arbitre arbitre;

	/**
	 * Change l'état d'une cellule si besoin
	 * @param x Absisse de le cellule à changer
	 * @param y Ordonné de la cellule à changer
	 * @param reference CellulesVivantes au début du tour qui sert de référence
	 */
	public abstract void changerCellule(int x, int y, CellulesVivantes reference);
	public abstract void inverserCellule(int x, int y);

	ActualiseurCellule(Arbitre arbitre) throws IllegalArgumentException{
		if(arbitre == null) {
			throw new IllegalArgumentException("L'arbitre ne peut pas être null!");
		}
		this.arbitre = arbitre;
	}

	public Arbitre getArbitre() {
		return arbitre;
	}
}
