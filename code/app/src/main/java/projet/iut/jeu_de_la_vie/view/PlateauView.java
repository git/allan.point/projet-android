package projet.iut.jeu_de_la_vie.view;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Build;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.view.WindowManager;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.view.MotionEventCompat;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Observable;

import projet.iut.jeu_de_la_vie.R;
import projet.iut.jeu_de_la_vie.model.cellule.Cellule;
import projet.iut.jeu_de_la_vie.model.cellule.Position;
import projet.iut.jeu_de_la_vie.model.cellule.observer.ObserverCellule;
import projet.iut.jeu_de_la_vie.model.cellulesVivantes.CellulesVivantes;
import projet.iut.jeu_de_la_vie.model.cellulesVivantes.observer.ObservableCV;
import projet.iut.jeu_de_la_vie.model.cellulesVivantes.observer.ObserverCV;

public class PlateauView extends View implements ObserverCV {
    private HashMap<Position, Integer> colors;

    public PlateauView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public PlateauView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public PlateauView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private Paint paint = new Paint();
    private int colones;
    private int lignes;
    private int dethColor = Color.BLACK;
    private int modif = 80;
    private boolean formule;

    public PlateauView(Context context, int colones, int lignes){
        super(context);
        this.colones = colones;
        this.lignes = lignes;
        int width = LauncherActivity.width;
        int height = LauncherActivity.height;
        formule = LauncherActivity.isPortrait ? width/colones < height/lignes : height/lignes >= width/colones;
        init();
    }
    public boolean isFormule(){
        return formule;
    }

    private void init(){
        colors = new HashMap<>();
        paint = new Paint();
        this.dethColor = Color.BLACK;
    }

    @Override
    public void onDraw(Canvas canvas){
        super.onDraw(canvas);
        Log.d("fragment","draw");
        Integer tmp;
        Rect rect;
        for(int i=0; i<lignes; ++i){
            for(int j=0; j<colones; ++j){
                Position p = new Position(j, i);
                tmp = colors.get(p);
                paint.setColor(dethColor);

                if(tmp instanceof Integer){
                    paint.setColor(tmp);
                }

                int width = LauncherActivity.width;
                int height = LauncherActivity.height;

                if (LauncherActivity.isPortrait) {
                    if (width/colones < height/lignes) {
                        rect = new Rect((width / colones) * j + (width / colones) * 5 / 100, width / colones * i + (width / colones) * 5 / 100, (width / colones * (j + 1)) - (width / colones) * 5 / 100, (width / colones * (i + 1)) - (width / colones) * 5 / 100);
                        canvas.drawRect(rect, paint);
                        Log.d("orientation", "portrait");
                        formule = true;
                    }
                    else {
                        rect = new Rect(((height-modif) / lignes) * j + ((height-modif) / lignes) * 5 / 100, (height-modif) / lignes * i + ((height-modif) / lignes) * 5 / 100, ((height-modif) / lignes * (j + 1)) - ((height-modif) / lignes) * 5 / 100, ((height-modif) / lignes * (i + 1)) - ((height-modif) / lignes) * 5 / 100);
                        canvas.drawRect(rect, paint);
                        Log.d("orientation", "portrait");
                        formule = false;
                    }
                }
                else {
                    if (height/lignes < width/colones) {
                        rect = new Rect((height - modif) / lignes * j + ((height - modif) / lignes) * 5 / 100, (height - modif) / lignes * i + ((height - modif) / lignes) * 5 / 100, ((height - modif) / lignes * (j + 1)) - ((height - modif) / lignes) * 5 / 100, ((height - modif) / lignes * (i + 1)) - ((height - modif) / lignes) * 5 / 100);
                        canvas.drawRect(rect, paint);
                        Log.d("orientation", "landscpape");
                        formule = false;
                    }
                    else{
                        rect = new Rect(width / colones * j + (width / colones) * 5 / 100, width / colones * i + (width / colones) * 5 / 100, (width / colones * (j + 1)) - (width / colones) * 5 / 100, (width / colones * (i + 1)) - (width / colones) * 5 / 100);
                        canvas.drawRect(rect, paint);
                        Log.d("orientation", "landscpape");
                        formule = true;
                    }
                }
            }
        }
    }


    @Override
    public void update(Cellule changingCell) {
        if(changingCell.isAlive()){
            colors.put(changingCell.getPosition(), changingCell.getActiveColor());
        }
        else {
            colors.remove(changingCell.getPosition());
        }
    }

    public void addPosCell(Cellule cellule){
        colors.put(cellule.getPosition(), cellule.getActiveColor());
    }
    public void delPosCell(Cellule cellule){
        colors.remove(cellule.getPosition());
    }
    public void resetMap(){ colors.clear(); }

    public void setColones(int value) throws IllegalArgumentException{
        if(value < 0){
            throw new IllegalArgumentException("La valeur des colones doit être supperieur à 0");
        }
        colones = value;
    }

    public void setLignes(int value) throws IllegalArgumentException {
        if (value < 0) {
            throw new IllegalArgumentException("La valeur des colones doit être supperieur à 0");
        }
        lignes = value;
    }
}
