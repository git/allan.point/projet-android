package projet.iut.jeu_de_la_vie.model.cellule;

/**
 * Représente une position dans un axe x et y (2 dimentions)
 * @author Yohann Breil
 * @author Allan Point
 */
public class Position {

	/**
	 * Position x
	 */
	private int x;

	/**
	 * Position y
	 */
	private int y;

	/**
	 *
	 * @param x position x
	 * @param y position y
	 */
	public Position(int x, int y){
		setX(x);
		setY(y);
	}

	/**
	 *
	 * @return position x
	 */
	public int getX() {
		return x;
	}

	/**
	 *
	 * @param valeur position x
	 */
	public void setX(int valeur){
		x = valeur;
	}

	/**
	 *
	 * @return position y
	 */
	public int getY() {
		return y;
	}

	/**
	 *
	 * @param valeur position y
	 */
	public void setY(int valeur) throws IllegalArgumentException{
		y = valeur;
	}

	/**
	 *
	 * @param o L'objet à comparer
	 * @return True si les 2 positions on les mêmes coordonées. Sinon false.
	 */
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Position position = (Position) o;
		return x == position.x && y == position.y;
	}

	@Override
	public int hashCode() {
		return (x*31 + y*41)/5;
	}
}
