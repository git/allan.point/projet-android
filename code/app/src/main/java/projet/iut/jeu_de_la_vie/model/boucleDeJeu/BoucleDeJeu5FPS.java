package projet.iut.jeu_de_la_vie.model.boucleDeJeu;

import projet.iut.jeu_de_la_vie.model.boucleDeJeu.observer.ObservableBDJ;

import java.util.LinkedList;

/**
 * Boucle de jeu qui notifit tout les 200ms pour avoir 5 Image Par Seconde dans un thread (fils d'execution) séparé.
 * @author Yohann Breui
 * @author Allan Point
 */
public class BoucleDeJeu5FPS extends ObservableBDJ implements IBoucleDeJeu {
		public BoucleDeJeu5FPS(){
			setObserveurs(new LinkedList<>());
		}

	/**
	 * Déffinition du comportement de la boucle de jeu pour le thread
	 */
	@Override
		public void run()
		{
			while (true){
				try {
					Thread.sleep(200);
					beep();
				}

				// Gestion des exceptions : si le processus à été intérompu pendant le sleep, on arrete la boucle.
				catch (InterruptedException e)
				{
					break;
				}
			}
		}

	/**
	 * Notifier les abonnés
	 */
	public void beep() {
			notifier();
		}
}
