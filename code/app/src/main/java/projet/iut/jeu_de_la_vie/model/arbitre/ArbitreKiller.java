package projet.iut.jeu_de_la_vie.model.arbitre;

import projet.iut.jeu_de_la_vie.model.cellulesVivantes.CellulesVivantes;
import projet.iut.jeu_de_la_vie.model.cellule.CellState;
import projet.iut.jeu_de_la_vie.model.plateau.Plateau;

/**
 * Arbitre qui tue toute les cellules
 * @author Yohann Breuil
 * @author Allan Point
 */
public class ArbitreKiller extends Arbitre{

	/**
	 *
	 * @param plateau Plateau à arbitrer
	 * @see Arbitre
	 */
	public ArbitreKiller(Plateau plateau) {
		super(plateau);
	}

	/**
	 *
	 * @param x Coordonée x de la cellule à checker
	 * @param y Coordonée y de la cellule à checker
	 * @param reference Toutes les cellules qui était vivantes au début du tour et qui servent donc de references
	 * @return L'état de la cellule au prohain tour
	 */
	@Override
	public CellState verifierChangementCellules(int x, int y, CellulesVivantes reference) {
		return CellState.DIE;
	}
}
