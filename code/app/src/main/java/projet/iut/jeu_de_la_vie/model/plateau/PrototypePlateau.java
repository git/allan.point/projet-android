package projet.iut.jeu_de_la_vie.model.plateau;

import androidx.fragment.app.Fragment;

import java.io.Serializable;

/**
 * Abstraction du clonnage de plateau
 * @author Yohann Breuil
 * @author Allan Point
 */
public interface PrototypePlateau {
	Plateau cloner();
}
