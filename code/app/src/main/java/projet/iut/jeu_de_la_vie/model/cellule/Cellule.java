package projet.iut.jeu_de_la_vie.model.cellule;

import android.graphics.Color;

import projet.iut.jeu_de_la_vie.model.cellule.observer.ObservableCellule;

/**
 * Classe métier représentant une cellule
 * @author Yohann Breuil
 * @author Allan Point
 */
public class Cellule extends ObservableCellule {
    /**
     * Position de la cellule
     * @see Position
     */
    private Position position;

    /**
     * État de la cellule
     */
    private boolean alive;

    /**
     * Permet d'avoir une propriété qui représente la coulleur de toutes les cellules vivantes
     */
    private static int livingColor;
        public static int getLivingColor() { return livingColor; }
        public static void setLivingColor(int color) { livingColor = color; }

    /**
     * Permet d'avoir une propriété qui représente la coulleur actuel d'une cellule en fonction de son etat
     */
    private int activeColor;
        public int getActiveColor() { return activeColor; }
        public void setActiveColor(int color) { activeColor =color; }

    /**
     * Couleur des cellules mortes
     */
    private int deathColor;
    public int getDeathColor(){
        return deathColor;
    }

    /**
     *
     * @param x position x de la cellule
     * @param y position y de la cellule
     * @throws IllegalArgumentException
     */
    public Cellule(int x, int y) throws IllegalArgumentException {
        deathColor = Color.BLACK;
        setActiveColor(deathColor);
        position = new Position(x,y);
        alive = false;
    }

    /**
     *
     * @return True si la cellule est vivante. Sinon false.
     */
    public Boolean isAlive() { return alive; }

    /**
     * Change l'état de la cellule en changant le couleur actve ainsi qu'en notifiant tout les abonnés du changement
     * @param alive Booléen assigné a l'état de la cellule
     */
    public void setAlive(Boolean alive) {
        setActiveColor(alive ? getLivingColor() : deathColor);
        this.alive = alive;
        notifier(this);
    }

    public Position getPosition(){
        return position;
    }

    /**
     *
     * @param o Objet à comparrer
     * @return True si les cellules ont les mêmes positions. Sinon false
     */
    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (o == this) return true;
        if (o.getClass() != this.getClass()) return false;

        if (position.getY() == ((Cellule) o).position.getY() && position.getX() == ((Cellule) o).position.getX() && isAlive() == ((Cellule) o).isAlive()) {
            return true;
        }
        return false;
    }

    /**
     * Inverset l'état d'une cellule. La tue si elle est vivante et vice versa.
     */
    public void inverseAlive(){
        setAlive(!alive);
    }
}
