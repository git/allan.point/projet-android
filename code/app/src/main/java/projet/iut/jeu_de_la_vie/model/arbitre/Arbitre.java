package projet.iut.jeu_de_la_vie.model.arbitre;

import projet.iut.jeu_de_la_vie.model.cellulesVivantes.CellulesVivantes;
import projet.iut.jeu_de_la_vie.model.CompteurDeCellule;
import projet.iut.jeu_de_la_vie.model.plateau.Plateau;
import projet.iut.jeu_de_la_vie.model.cellule.CellState;

/**
 * @author Yohann Breuil
 * @author Allan Point
 */
public abstract class Arbitre {

	private Plateau plateau;
	private CompteurDeCellule compteurCell;

	/**
	 * @param plateau Plateau à arbitrer
	 * @throws IllegalArgumentException Lève une exception si le plateau est null
	 */
	public Arbitre(Plateau plateau) throws IllegalArgumentException {
		if(plateau == null){
			throw new IllegalArgumentException("Le plateau ne doit pat être null");
		}
		this.plateau = plateau;
		compteurCell = new CompteurDeCellule();
	}

	/**
	 *
	 * @return Le plateau en cours d'arbitrage
	 */
	public Plateau getPlateau(){
		return plateau;
	}

	/**
	 *
	 * @return Le compteur de cellule du jeu
	 * @see CompteurDeCellule
	 */
	protected CompteurDeCellule getCompteurCell(){
		return compteurCell;
	}

	/**
	 *
	 * @param x Coordonée x de la cellule à checker
	 * @param y Coordonée y de la cellule à checker
	 * @param reference Toutes les cellules qui était vivantes au début du tour et qui servent donc de references
	 * @return L'état de la cellule au prochain tours
	 * @see CellState
	 */
	public abstract CellState verifierChangementCellules(int x, int y, CellulesVivantes reference);

	public void setPlateau (Plateau plateau){
		this.plateau = plateau;
	}

}
