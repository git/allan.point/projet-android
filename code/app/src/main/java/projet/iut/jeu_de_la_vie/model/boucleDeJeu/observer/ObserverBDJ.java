package projet.iut.jeu_de_la_vie.model.boucleDeJeu.observer;

import java.io.Serializable;

/**
 * Permet d'obbserver une boucle de jeu
 * @author Yohann Breuil
 * @author Allan Point
 */
public interface ObserverBDJ extends Serializable {
	/**
	 * Réaction en cas de notification
	 */
	void update();
}
