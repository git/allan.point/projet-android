package projet.iut.jeu_de_la_vie.model.cellule.observer;

import projet.iut.jeu_de_la_vie.model.cellule.Cellule;

import java.util.*;

/**
 * Permet à une cellule d'être observée
 * @author Yohann Breil
 * @author Allan Point
 */
public abstract class ObservableCellule {
	/**
	 * Liste des observeurs de la boucle de jeu. Cette liste servira à notifier les abonnés de la boucle de jeu
	 */
	List<ObserverCellule> observeurs;

	public ObservableCellule(){
		observeurs = new LinkedList<>();
	}

	/**
	 *
	 * @param o observeur à attacher
	 * @throws IllegalArgumentException L'observeur ne peut pas être null
	 */
	public void attacher(ObserverCellule o) throws  IllegalArgumentException{
		if(o == null){
			throw new IllegalArgumentException("L'observer ne doit pas être null");
		}
		observeurs.add(o);
	}

	/**
	 *
	 * @param o observeur à détacher
	 * @throws IllegalArgumentException L'observer ne peut pas être null
	 */
	public void detacher(ObserverCellule o) throws IllegalArgumentException{
		if(o == null){
			throw new IllegalArgumentException("L'observer ne doit pas être null");
		}
		observeurs.remove(o);
	}

	/**
	 * Permet de notifier les abonnés de la boucle de jeu
	 */
	public void notifier(Cellule cellule) {
		for (ObserverCellule observeur : observeurs) {
			observeur.update(cellule);
		}
	}
}
