package projet.iut.jeu_de_la_vie.model.actualiseur;

import projet.iut.jeu_de_la_vie.model.cellulesVivantes.CellulesVivantes;
import projet.iut.jeu_de_la_vie.model.arbitre.Arbitre;

/**
 * Permet de changer l'état d'une cellule si besoin
 * @author Yohann Breuil
 * @author Allan Point
 */
public class ActualiseurEtatCellule extends ActualiseurCellule{
	public ActualiseurEtatCellule(Arbitre a) throws IllegalArgumentException{
		super(a);
	}

	/**
	 * Peremet de changer l'état d'une cellule si besoin
	 * @param x Absisse de le cellule à changer
	 * @param y Ordonné de la cellule à changer
	 * @param reference CellulesVivantes au début du tour qui sert de référence
	 */
	@Override
	public void changerCellule(int x, int y, CellulesVivantes reference) {
		switch(getArbitre().verifierChangementCellules(x, y, reference)) {
			case DIE:
				getArbitre().getPlateau().getCell(x, y).setAlive(false);
				break;
			case LIVE:
			case BIRTH:
				getArbitre().getPlateau().getCell(x, y).setAlive(true);
				break;
		}
	}

	@Override
	public void inverserCellule(int x, int y) {
		getArbitre().getPlateau().inverserCellule(x, y);
	}
}
