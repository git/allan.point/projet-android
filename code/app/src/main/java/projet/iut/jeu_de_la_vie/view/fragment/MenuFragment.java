package projet.iut.jeu_de_la_vie.view.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import projet.iut.jeu_de_la_vie.R;
import projet.iut.jeu_de_la_vie.model.cellule.Cellule;
import projet.iut.jeu_de_la_vie.view.LauncherActivity;

public class MenuFragment extends Fragment {
    public MenuFragment() {
        super(R.layout.menu_principal);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.findViewById(R.id.startButton).setOnClickListener(l->{startGame(view);});
        Log.d("fragment",""+view);
    }

    private int getNbColumnsFromView(View v) throws NumberFormatException{
        EditText editText = v.findViewById(R.id.nbColone);
        String colones = editText.getText().toString();
        Log.d("fragment","ICI : "+colones);
        return Integer.parseInt(colones);
    }
    private int getNbLignesFromView(View v) throws NumberFormatException{
        EditText editText = v.findViewById(R.id.nbLignes);
        String lignes = editText.getText().toString();
        Log.d("fragment","ICI : "+lignes);
        return Integer.parseInt(lignes);
    }
    public void startGame(View view){
        Bundle bundle = new Bundle();
        int colones;
        int lignes;
        try {
            lignes = getNbLignesFromView(view);
            colones = getNbColumnsFromView(view);
        }catch (NumberFormatException e){
            lignes=10;
            colones=10;
        }
        Log.d("fragment","lignes"+lignes);
        Log.d("fragment","colonnes"+colones);
        bundle.putInt("lignes", lignes);
        bundle.putInt("colones", colones);
        ((LauncherActivity)getActivity()).startGame(bundle);
    }
}
