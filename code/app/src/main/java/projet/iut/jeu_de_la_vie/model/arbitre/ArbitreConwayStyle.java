package projet.iut.jeu_de_la_vie.model.arbitre;

import projet.iut.jeu_de_la_vie.model.cellulesVivantes.CellulesVivantes;
import projet.iut.jeu_de_la_vie.model.plateau.Plateau;
import projet.iut.jeu_de_la_vie.model.cellule.CellState;

/**
 * Arbitre selon les régles de Conway (3 voisinnes pour naitre, 2 ou 3 voisinnes pour survivre, meurt dans d'autre situations)
 * @author Yohann Breuil
 * @author Allan Point
 */
public class ArbitreConwayStyle extends Arbitre{

	/**
	 *
	 * @param plateau Plateau à arbitrer
	 * @see Arbitre
	 */
	public ArbitreConwayStyle(Plateau plateau) {
		super(plateau);
	}

	/**
	 *
	 * @param x Coordonée x de la cellule à checker
	 * @param y Coordonée y de la cellule à checker
	 * @param reference Toutes les cellules qui était vivantes au début du tour et qui servent donc de references
	 * @return L'état de la cellule au prohain tour
	 */
	@Override
	public CellState verifierChangementCellules(int x, int y, CellulesVivantes reference) {
		if(verifierNaissance(x, y, reference)) {
			return CellState.BIRTH;
		}
		if(verifierMort(x, y, reference)) {
			return CellState.DIE;
		}
		return reference.getAt(x, y) != null ? CellState.LIVE : CellState.DIE;
	}

	/**
	 *
	 * @param x Coordonée x de la cellule à checker
	 * @param y Coordonée y de la cellule à checker
	 * @param reference Toutes les cellules qui était vivantes au début du tour et qui servent donc de references
	 * @return True si la cellule doit naître. Sinon false
	 */
	private boolean verifierNaissance(int x, int y, CellulesVivantes reference) {
		int cpt = getCompteurCell().compteNombreCellulesAutour(x, y, reference);
		if(cpt == 3 && !getPlateau().getCell(x, y).isAlive()) {
		 	return true;
		}
		return false;
	}
	/**
	 *
	 * @param x Coordonée x de la cellule à checker
	 * @param y Coordonée y de la cellule à checker
	 * @param reference Toutes les cellules qui était vivantes au début du tour et qui servent donc de references
	 * @return True si la cellule doit mourir. Sinon false
	 */
	private boolean verifierMort(int x, int y, CellulesVivantes reference) {
		int cpt = getCompteurCell().compteNombreCellulesAutour(x, y, reference);
		if(!(cpt == 2 || cpt == 3) && getPlateau().getCell(x, y).isAlive()) {
		 	return true;
		}
		return false;
	}


}
