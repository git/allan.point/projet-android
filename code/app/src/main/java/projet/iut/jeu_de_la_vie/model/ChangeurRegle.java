package projet.iut.jeu_de_la_vie.model;

import projet.iut.jeu_de_la_vie.model.actualiseur.ActualiseurCellule;
import projet.iut.jeu_de_la_vie.model.actualiseur.ActualiseurEtatCellule;
import projet.iut.jeu_de_la_vie.model.arbitre.ArbitreConwayStyle;
import projet.iut.jeu_de_la_vie.model.arbitre.ArbitreKiller;
import projet.iut.jeu_de_la_vie.model.plateau.Plateau;

/**
 * Permet de gerer le changement de règles
 * @author Yohann Breuil
 * @author Allan Point
 */
public class ChangeurRegle {
	private Regle regleEnCours;
		public Regle getRegleEnCours() { return regleEnCours;	}
		public void setRegleEnCours(Regle regleEnCours) { this.regleEnCours = regleEnCours; }

	public ChangeurRegle(){
			setRegleEnCours(Regle.values()[0]);
	}

	/**
	 * Change l'actualiseur en fonction des règles
	 * @param plateau Plateau actuel du jeu
	 * @return Un ActualiseurCellule avec le bon arbitre
	 * @see projet.iut.jeu_de_la_vie.model.arbitre.Arbitre
	 * @see ActualiseurCellule
	 */
	public ActualiseurCellule changerRegle(Plateau plateau) {
		switch (getRegleEnCours()){
			case REGLE_KILLER:
				return new ActualiseurEtatCellule(new ArbitreKiller(plateau));
			default:
				return new ActualiseurEtatCellule(new ArbitreConwayStyle(plateau));
			}
		}
}
