package projet.iut.jeu_de_la_vie.model;

import projet.iut.jeu_de_la_vie.model.cellule.Cellule;
import projet.iut.jeu_de_la_vie.model.cellule.Position;
import projet.iut.jeu_de_la_vie.model.cellulesVivantes.CellulesVivantes;

import java.util.LinkedList;
import java.util.List;

/**
 * Classe permtant de compter des cellules
 * @author Yohann Breuil
 * @author Allan Point
 */
public class CompteurDeCellule {

	/**
	 * Compte le nombre de voisinne de la cellule (x; y)
	 * @param x Absisse de la cellule à compter
	 * @param y Ordoné de la cellule à compter
	 * @param cellulesVivantes Toutes les cellule vivantes
	 * @return Le nombre de voisinne de la cellule (x; y)
	 */
	public int compteNombreCellulesAutour(int x, int y, CellulesVivantes cellulesVivantes){
		int cpt = 0;
		Cellule c;
		List<Position> positionsAVerifier = new LinkedList<>();

		// Définition des 8 diréction autour de la position (x; y)
		positionsAVerifier.add(new Position(x-1, y-1));
		positionsAVerifier.add(new Position(x, y-1));
		positionsAVerifier.add(new Position(x+1, y-1));
		positionsAVerifier.add(new Position(x+1, y));
		positionsAVerifier.add(new Position(x+1, y+1));
		positionsAVerifier.add(new Position(x, y+1));
		positionsAVerifier.add(new Position(x-1, y+1));
		positionsAVerifier.add(new Position(x-1, y));

		for (Position p: positionsAVerifier) {
			c = cellulesVivantes.getAt(p.getX(), p.getY());
			cpt = c == null ? cpt : cpt+1;
		}
		return cpt;
	}
}
