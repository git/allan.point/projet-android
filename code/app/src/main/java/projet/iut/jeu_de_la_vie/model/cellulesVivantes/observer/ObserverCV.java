package projet.iut.jeu_de_la_vie.model.cellulesVivantes.observer;

import projet.iut.jeu_de_la_vie.model.cellule.Cellule;

public interface ObserverCV {
    void update(Cellule changingCell);
}
