package projet.iut.jeu_de_la_vie.model.cellulesVivantes.observer;

import java.util.LinkedList;
import java.util.List;

import projet.iut.jeu_de_la_vie.model.cellule.Cellule;

public abstract class ObservableCV {
    private List<ObserverCV> observeurs;

    public ObservableCV(){observeurs = new LinkedList<>();}

    public void attacher(ObserverCV o) throws  IllegalArgumentException{
        if(o == null){
            throw new IllegalArgumentException("L'observer ne doit pas être null");
        }
        observeurs.add(o);
    }

    public void detacher(ObserverCV o) throws IllegalArgumentException{
		if(o == null){
			throw new IllegalArgumentException("L'observer ne doit pas être null");
		}
		observeurs.remove(o);
	}
	public void notifier(Cellule changingCell) {
		for (ObserverCV observeur : observeurs) {
			observeur.update(changingCell);
		}
	}

}
