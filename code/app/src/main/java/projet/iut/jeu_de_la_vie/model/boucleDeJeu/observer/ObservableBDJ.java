package projet.iut.jeu_de_la_vie.model.boucleDeJeu.observer;

import java.util.List;

/**
 * @author Yohann Breuil
 * @author Allan Point
 */
public abstract class ObservableBDJ {
	/**
	 * Liste des observeurs de la boucle de jeu. Cette liste servira à notifier les abonnés de la boucle de jeu
	 */
	private List<ObserverBDJ> observeurs;

	/**
	 *
	 * @return Les observeur de la boucle observée
	 */
	protected List<ObserverBDJ> getObserveurs(){
		return observeurs;
	}

	protected void setObserveurs(List<ObserverBDJ> valeur){
		observeurs = valeur;
	}

	/**
	 * Permet de s'abonner  l'observable
	 * @param o Observateur qui veut s'abboner
	 */
	public void attacher(ObserverBDJ o) {
		observeurs.add(o);
	}

	/**
	 * Permet de se déabonner de la liste de notification de la boucle.
	 * @param o Observateur qui veut s'abbonner
	 */
	public void detacher(ObserverBDJ o){
		observeurs.remove(o);
	}

	/**
	 * Permet de notifier les abonnés de la boucle de jeu
	 */
	protected void notifier() {
		/*
		for (ObserverBDJ observeur : observeurs) {
			Platform.runLater(()->observeur.update());
		}
		 */
		for (ObserverBDJ observeur : observeurs) {
			observeur.update();
		}
	}
}