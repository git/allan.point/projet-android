package projet.iut.jeu_de_la_vie.model.cellule.créateur;


import projet.iut.jeu_de_la_vie.model.cellulesVivantes.CellulesVivantes;
import projet.iut.jeu_de_la_vie.model.cellule.Cellule;

import java.util.LinkedList;
import java.util.List;

/**
 * Permet de créer des cellules de manières sécurisé et standardiser
 * @author Yohann Breuil
 * @author Allan Point
 */
public class CreateurCellule implements ICreateurCellule {
	/**
	 * Nombre de colones à créer
	 */
	private int w;

	/**
	 * Nombre de ligne à créer
	 */
	private int h;

	/**
	 *
	 * @param w Nombre de colones. Doit être supperieur ou égal à 0.
	 * @param h Nombre de lignes. Doit être supperieur ou égal à 0.
	 * @throws IllegalArgumentException Si w ou h sont strictement inferieur à 0.
	 */
	public CreateurCellule(int w, int h) throws IllegalArgumentException{
		if(w<0 || h<0){
			throw new IllegalArgumentException("La longueur et la largeur doivent être supperieur à 0");
		}
		this.w = w;
		this.h = h;
	}

	/**
	 * Créer des cellules selon les dimentions précisé dans le constructeur.
	 * @param observer Permet d'abonner un objet CellulesVivantes à toute les cellules.
	 * @return Une liste observable pour fxml avec toutes les cellules standardisées  .
	 */
	public List<List<Cellule>> creerCellules(CellulesVivantes observer){
		return creerCellules(w, h, observer);
	}

	/**
	 * Créer des cellules.
	 * @param colone Nombre de colones à créer.
	 * @param ligne Nombre de lignes à créer.
	 * @param observer Permet d'abonner un objet CellulesVivantes à toute les cellules.
	 * @return Une liste observable pour fxml avec toutes les cellules standardisées.
	 */
	public List<List<Cellule>> creerCellules(int colone, int ligne, CellulesVivantes observer){
		List<List<Cellule>> cells = new LinkedList<>();
		List<Cellule> tmp;
		Cellule c;
		for (int i = 0; i < ligne; i++) {
			tmp = new LinkedList<>();
			for (int j = 0; j < colone; j++) {
				c = new Cellule(j, i);
				c.attacher(observer);
				tmp.add(c);
			}
			cells.add(tmp);
		}
		return cells;
	}

	/**
	 * Créer une ligne de cellule
	 * @param ligne nombre de ligne à créer
	 * @return Les lignes avec les cellules.
	 */
	public List<Cellule> creerLigneCellule(int ligne){
		List<Cellule> cells = new LinkedList<>();
		for(int i=0; i<ligne; ++i){
			cells.add(new Cellule(i, ligne));
		}
		return cells;
	}
}
