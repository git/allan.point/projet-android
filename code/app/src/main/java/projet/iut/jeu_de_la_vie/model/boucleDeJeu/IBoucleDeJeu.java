package projet.iut.jeu_de_la_vie.model.boucleDeJeu;

/**
 * Perrmet d'avoir une boucle de jeu Runnable dans un thread
 * @author Yohann Breuil
 * @author Allan Point
 */
public interface IBoucleDeJeu extends Runnable {}
