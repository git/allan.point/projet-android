# Le jeu de la vie

## Principe

Le jeu de la vie est un jeu à 0 joueur on l'on a un ensemble de
cellules sur un monde.
Ces cellules sont soit morte, soit vivante. 
Le but du jeu est de placer des cellules dans le monde et de voir
leurs évolution au cours du temps.

## Histoire

Le jeu de la vie a été inventé en 1970 par le mathématicien John H. Conwaty.

## Structure

Il esxiste différentes structure stable ou périodique.


En voici quelques exemples:

### Le glider

![Un glider](https://vignette.wikia.nocookie.net/emergentuniverse/images/d/d0/Game_of_life_animated_glider_2.gif/revision/latest?cb=20120305021401)

### Les cannons

![Un cannon](https://conwaylife.com/w/images/b/b6/Gosperglidergun.gif)

### Et bien d'autres

![Un puffer](www.microsiervos.com/images/puffertr.gif)

## Comment y jouer

### Les regles

Il existe un vaste nombre de de regles. Toutefois, nous avons choisi de 
n'implémenter que les regles de bases imaginé par J. H. Conway:

* Une celle nait si elle a exactement 3 voisinnes
* Une celle reste en vie si elle a 2 ou 3 voisines
* Dans les autres cas, elle meurt

## Notice

### Le menu

Dans le menu, on peut choisir :
* Le nombre de colones du monde
* Le nombre de ligne du monde
* La couleur dess cellules lors ce qu'elles sont vivantes.

Une fois ces éléments réglé selon ses convenances, on peut lancer le jeu avec le
bouton `start \(o^u^o)/` en bas du menu

### Le jeu

Une fois qu'on a réglé les paramètre dans le menu, on accède à l'écran de jeu.
Quand on arrive a cette étape, on peut chosir de fair vivre une cellule en tappant
sur son emplacement sur l'écran. Une fois qu'on est prêt·e, il suffit d'apuier sur
le boutton `Start` pour lancer le jeu et voir l'évolution de la population
de cellules.

## Le public cible

Il n'y a pas de public ciblé, ne serait-ce selement le plus grand nombre de
personnes. Toutefois, l'accéssibilité à tout·e·s n'a pas était pris en compte
dans le dévelopemment.

## Utilité

Le seul but de ce cette application est de permettre au gents de pouvoir 
avoir un jeu de la vie sur eux·lles.

