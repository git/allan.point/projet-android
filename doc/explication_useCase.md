# Explication du diagrame de classe

## Les acteur·ice·s

Une seule catégorie de perssonne est sensé interagire avec le system: ce sont
les joueur·euse·s

## Les cas d'utilisations

Il y a plusieurs cas du'ilisation si on choisi une granularité faible:

* Jouer: L'acteur·ice peut jouer au jeu
  * Lancer le jeu: L'acteur·ice peu lancer le jeu et observer l'évolution de la population de cellules
  * Pause: L'acteur·ice peut choisir de mettre en pause le jeu en stopant la boucle de jeu
  * Placer une cellule: L'acteur·ice peut choisir de placer une cellule sur le plateau
* Parametrer: L'acteur·ice doit parametrer le monde avant de jouer
  * Choisir le nombre de lignes
  * Choisir le nombre de colonnes
  * Choisir la couleur des cellules vivantes
