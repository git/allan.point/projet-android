# Eplication du diagrame de classe

## Le model

### Les cellules

[Cette classe](../code/app/src/main/java/projet/iut/jeu_de_la_vie/model/cellule/Cellule.java) représente un cellule. Elles sont caracterisé par des coordonés et elles partages la couleur de vie. Elles sont observables.

### Le plateau

Le [plateau](../code/app/src/main/java/projet/iut/jeu_de_la_vie/model/plateau/Plateau.java) représente le mode et stock les cellules.

### Les cellules vivantes

La classe [CelluleVivante](../code/app/src/main/java/projet/iut/jeu_de_la_vie/model/cellulesVivantes/CellulesVivantes.java) permmet de stoker les celules vivantes sur un tour. Elle permet de faciliter et d'optimiser le calcule de la prochaine génération.

### L'abitre

L'[arbitre](../code/app/src/main/java/projet/iut/jeu_de_la_vie/model/arbitre/) décide de tuer, faire naître ou de laisser vivre les celules.

### L'actualiseur

L'[actualiseur](../code/app/src/main/java/projet/iut/jeu_de_la_vie/model/actualiseur/) permet de déciser d'actualiser la logique du jeu. Il appèle notament un Arbitre.

### Le manager

Le [Manager](../code/app/src/main/java/projet/iut/jeu_de_la_vie/model/Manager.java) permet d'avoir une interface pour demender des requettes au modèle.

## Les vues

Nous avons une seul [vue principale](../code/app/src/main/res/layout/jeu_de_la_vie.xml) qui contient un fragment, nous permettant d'alterner entre le [menu principal](../code/app/src/main/res/layout-land/menu_principal.xml) et la [vue du jeu](../code/app/src/main/res/layout/fragment_plateau.xml) qui est mise à jour par la classe [PlateauView](../code/app/src/main/java/projet/iut/jeu_de_la_vie/view/PlateauView.java).

### Les fragments

Nous utilisons 2 fragments: le [fragment pour le menu principal](../code/app/src/main/java/projet/iut/jeu_de_la_vie/view/fragment/MenuFragment.java) qui affiche le menu permettant de changer les paramètres du jeu et le [fragment du jeu](../code/app/src/main/java/projet/iut/jeu_de_la_vie/view/fragment/FragmentPlateau.java) qui gère les intéractions entre l'utilisateur et la vue du jeu.

#### Le menu

Il y a un [fragment pour le menu principal](../code/app/src/main/java/projet/iut/jeu_de_la_vie/view/fragment/MenuFragment.java) qui permet de personaliser l'interface de jeu. Ce fragment est lié à [LauncherActivity](../code/app/src/main/java/projet/iut/jeu_de_la_vie/view/LauncherActivity.java)

#### L'interface de jeu

Permet d'avoir une vision du jeu grace à une grille de réctangle. Aussi, elle permet de lancer / recommencer le jeu. Il est lié à [PlateauView](../code/app/src/main/java/projet/iut/jeu_de_la_vie/view/PlateauView.java)

### Les activity

On a qu'une seule activitée qui est [LaucherActivity](../code/app/src/main/java/projet/iut/jeu_de_la_vie/view/LauncherActivity.java). Elle est lancée au lancement de l'application et permet de chargé les éléments naicéssaire à l'app.

